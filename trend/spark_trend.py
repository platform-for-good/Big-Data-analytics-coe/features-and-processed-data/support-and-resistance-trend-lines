#spark-submit spark_trend.py -t 'msft' -p -60 -s ../../../storage/s1/securities/1d/ -d ../../../storage/s2/trend/
import trendln
from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, IntegerType, DateType, DoubleType
from argparse import ArgumentParser
import pandas as pd

parser = ArgumentParser(description='Trend')
parser.add_argument('-t', '--ticker', default='msft', type=str, required=False, help='Used to look up a specific ticker.')
parser.add_argument('-p', '--period', default=-60, type=int, required=False, help='Number of day to look back from last day')
parser.add_argument('-s', '--src', default='../../../storage/s1/securities/1d/', type=str, required=False, help='Source directory of the data to be found.')
parser.add_argument('-d', '--dest', default='../../../storage/s2/trend/', type=str, required=False, help='Destination directory of the processed to be stored.')

args = parser.parse_args()
print("Ticker " + args.ticker)
print("Source " + args.src)
print("Destination " + args.dest)

spark = SparkSession.builder.appName("trend").getOrCreate()

ticker = args.ticker
path = args.src + ticker + '.csv'
accuracy = 6
numberOfDays = args.period
format = "png"
fileName = ticker + "." + format

DATE = 'Date'
OPEN = 'Open'
HIGH = 'High'
LOW = 'Low'
CLOSE = 'Close'
ADJUSTED_CLOSE = 'Adj Close'
VOLUME = 'Volume'

schema = (StructType([
    StructField(DATE,DateType(),True),               # 0
    StructField(OPEN,DoubleType(),True),             # 1
    StructField(HIGH,DoubleType(),True),             # 2
    StructField(LOW,DoubleType(),True),              # 3
    StructField(CLOSE,DoubleType(),True),            # 4
    StructField(ADJUSTED_CLOSE,DoubleType(),True),   # 5
    StructField(VOLUME,IntegerType(),True)           # 6
    ]))
df = spark.read.format('csv') \
                .schema(schema) \
                .option("header","true") \
                .option("sep",",") \
                .load(path)

pdf = df.toPandas()
print(pdf)

hist = pdf.iloc[numberOfDays:, :]
histClose = hist.loc[:, CLOSE].to_numpy()
# print("histClose")
# print(type(histClose[0]))
# print(histClose)

histHigh = hist.loc[:, HIGH].to_numpy()
histLow = hist.loc[:, LOW].to_numpy()

idx = pd.DatetimeIndex(hist.loc[:, DATE])
print("idx")
print(idx)

# (minimaIdxs, pmin, mintrend, minwindows), (maximaIdxs, pmax, maxtrend, maxwindows) = \
#     trendln.calc_support_resistance(
        # list/numpy ndarray/pandas Series of data as bool/int/float and if not a list also unsigned
        # or 2-tuple (support, resistance) where support and resistance are 1-dimensional array-like or one or the other is None
        # can calculate only support, only resistance, both for different data, or both for identical data
        # h,

        # METHOD_NAIVE - any local minima or maxima only for a single interval (currently requires pandas)
        # METHOD_NAIVECONSEC - any local minima or maxima including those for consecutive constant intervals (currently requires pandas)
        # METHOD_NUMDIFF (default) - numerical differentiation determined local minima or maxima (requires findiff)
        # extmethod=METHOD_NUMDIFF,

        # METHOD_NCUBED - simple exhuastive 3 point search (slowest)
        # METHOD_NSQUREDLOGN (default) - 2 point sorted slope search (fast)
        # METHOD_HOUGHPOINTS - Hough line transform optimized for points
        # METHOD_HOUGHLINES - image-based Hough line transform (requires scikit-image)
        # METHOD_PROBHOUGH - image-based Probabilistic Hough line transform (requires scikit-image)
        # method=METHOD_NSQUREDLOGN,

        # window size when searching for trend lines prior to merging together
        # window=125,

        # maximum percentage slope standard error
        # errpct=0.005,

        # for all METHOD_*HOUGH*, the smallest unit increment for discretization e.g. cents/pennies 0.01
        # hough_scale=0.01,

        # only for METHOD_PROBHOUGH, number of iterations to run
        # hough_prob_iter=10,

        # sort by area under wrong side of curve, otherwise sort by slope standard error
        # sortError=False,

        # accuracy if using METHOD_NUMDIFF for example 5-point stencil is accuracy=3
        # accuracy=1)
# if h is a 2-tuple with one value as None, then a 2-tuple is not returned, but the appropriate tuple instead
# minimaIdxs - sorted list of indexes to the local minima
# pmin - [slope, intercept] of average best fit line through all local minima points
# mintrend - sorted list containing (points, result) for local minima trend lines
# points - list of indexes to points in trend line
# result - (slope, intercept, SSR, slopeErr, interceptErr, areaAvg)
# slope - slope of best fit trend line
# intercept - y-intercept of best fit trend line
# SSR - sum of squares due to regression
# slopeErr - standard error of slope
# interceptErr - standard error of intercept
# areaAvg - Reimann sum area of difference between best fit trend line
#   and actual data points averaged per time unit
# minwindows - list of windows each containing mintrend for that window

# maximaIdxs - sorted list of indexes to the local maxima
# pmax - [slope, intercept] of average best fit line through all local maxima points
# maxtrend - sorted list containing (points, result) for local maxima trend lines
# see for mintrend above
# maxwindows - list of windows each containing maxtrend for that window
#returns (list of minima indexes, list of maxima indexes, [support slope coefficient, intersect], [resistance slope coefficient, intersect], [[support point indexes], (slope, intercept, residual, slope error, intercept error, area on wrong side of trend line per time unit)]

# The get_extrema function will calculate all of the local minima and local maxima without performing the full trend line calculation.

mins, maxs = trendln.calc_support_resistance(histClose, accuracy = accuracy)
print("On close price: calc_support_resistance")
print("mins")
print(mins)
print("maxs")
print(maxs)

total = len(mins[0])
for i in range(total):
    # print(i)
    # print(mins[0][i])
    print(idx[mins[0][i]])
    print(histClose[mins[0][i]])

minimaIdxs, pmin, mintrend, minwindows = trendln.calc_support_resistance((histLow, None), accuracy = accuracy) #support only
print("On low price: calc_support_resistance")
print("minimaIdxs")
print(minimaIdxs)
print("pmin")
print(pmin)
print("mintrend")
print(mintrend)
print("minwindows")
print(minwindows)

mins, maxs = trendln.calc_support_resistance((histLow, histHigh), accuracy = accuracy)
print("On low and high price: calc_support_resistance")
print("mins")
print(mins)
print("maxs")
print(maxs)

(minimaIdxs, pmin, mintrend, minwindows), (maximaIdxs, pmax, maxtrend, maxwindows) = mins, maxs
minimaIdxs, maximaIdxs = trendln.get_extrema(histClose, accuracy = accuracy)
print("On close price: get_extrema")
print("minimaIdxs")
print(minimaIdxs)
print("maximaIdxs")
print(maximaIdxs)

# The plot_support_resistance function will calculate and plot the average and top 2 support and resistance lines, along with marking extrema used with a maximum history length, and otherwise identical arguments to the calculation function.
# plt = trendln.plot_support_resistance(hist[numberOfDays:].Close, accuracy = accuracy) # requires matplotlib - pip install matplotlib
# plt.savefig("close_" + fileName, format=format)
# plt.show()
# plt.clf() #clear figure

maximaIdxs = trendln.get_extrema((None, histHigh), accuracy = accuracy) #maxima only
print("On high price: get_extrema")
print("minimaIdxs")
print(minimaIdxs)
print("maximaIdxs")
print(maximaIdxs)

minimaIdxs, maximaIdxs = trendln.get_extrema((histLow, histHigh), accuracy = accuracy)
print("On low price: get_extrema")
print("minimaIdxs")
print(minimaIdxs)
print("maximaIdxs")
print(maximaIdxs)

# The plot_sup_res_date function will do the same as plot_support_resistance with help for nice formatting of dates based on a pandas date index.
# idx = pd.DatetimeIndex(hist.loc[:, DATE])
# print("idx")
# print(idx)

plt = trendln.plot_sup_res_date(histClose, idx, accuracy = accuracy) #requires pandas
plt.savefig("close_" + fileName, format=format)
plt.show()
plt.clf() #clear figure
#
plt = trendln.plot_sup_res_date((histLow, histHigh), idx, accuracy = accuracy) #requires pandas
plt.savefig("low_high_" + fileName, format=format)
plt.show()
plt.clf() #clear figure
