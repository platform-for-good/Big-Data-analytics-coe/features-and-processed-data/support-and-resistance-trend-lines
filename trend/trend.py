import trendln
# this will serve as an example for security or index closing prices, or low and high prices
import yfinance as yf # requires yfinance - pip install yfinance

accuracy = 6
numberOfDays = -60
ticker = 'msft'
format = "png"
fileName = ticker + "." + format

tick = yf.Ticker(ticker)
hist = tick.history(period="max", rounding=True)
print(type(hist))
print(hist)

h = hist[numberOfDays:].Close
print("h")
print(type(h[0]))
print(h)

# The plot_sup_res_date function will do the same as plot_support_resistance with help for nice formatting of dates based on a pandas date index.
idx = hist[numberOfDays:].index
print("idx")
print(type(idx[0]))
print(idx)

# (minimaIdxs, pmin, mintrend, minwindows), (maximaIdxs, pmax, maxtrend, maxwindows) = \
#     trendln.calc_support_resistance(
        # list/numpy ndarray/pandas Series of data as bool/int/float and if not a list also unsigned
        # or 2-tuple (support, resistance) where support and resistance are 1-dimensional array-like or one or the other is None
        # can calculate only support, only resistance, both for different data, or both for identical data
        # h,

        # METHOD_NAIVE - any local minima or maxima only for a single interval (currently requires pandas)
        # METHOD_NAIVECONSEC - any local minima or maxima including those for consecutive constant intervals (currently requires pandas)
        # METHOD_NUMDIFF (default) - numerical differentiation determined local minima or maxima (requires findiff)
        # extmethod=METHOD_NUMDIFF,

        # METHOD_NCUBED - simple exhuastive 3 point search (slowest)
        # METHOD_NSQUREDLOGN (default) - 2 point sorted slope search (fast)
        # METHOD_HOUGHPOINTS - Hough line transform optimized for points
        # METHOD_HOUGHLINES - image-based Hough line transform (requires scikit-image)
        # METHOD_PROBHOUGH - image-based Probabilistic Hough line transform (requires scikit-image)
        # method=METHOD_NSQUREDLOGN,

        # window size when searching for trend lines prior to merging together
        # window=125,

        # maximum percentage slope standard error
        # errpct=0.005,

        # for all METHOD_*HOUGH*, the smallest unit increment for discretization e.g. cents/pennies 0.01
        # hough_scale=0.01,

        # only for METHOD_PROBHOUGH, number of iterations to run
        # hough_prob_iter=10,

        # sort by area under wrong side of curve, otherwise sort by slope standard error
        # sortError=False,

        # accuracy if using METHOD_NUMDIFF for example 5-point stencil is accuracy=3
        # accuracy=1)
# if h is a 2-tuple with one value as None, then a 2-tuple is not returned, but the appropriate tuple instead
# minimaIdxs - sorted list of indexes to the local minima
# pmin - [slope, intercept] of average best fit line through all local minima points
# mintrend - sorted list containing (points, result) for local minima trend lines
# points - list of indexes to points in trend line
# result - (slope, intercept, SSR, slopeErr, interceptErr, areaAvg)
# slope - slope of best fit trend line
# intercept - y-intercept of best fit trend line
# SSR - sum of squares due to regression
# slopeErr - standard error of slope
# interceptErr - standard error of intercept
# areaAvg - Reimann sum area of difference between best fit trend line
#   and actual data points averaged per time unit
# minwindows - list of windows each containing mintrend for that window

# maximaIdxs - sorted list of indexes to the local maxima
# pmax - [slope, intercept] of average best fit line through all local maxima points
# maxtrend - sorted list containing (points, result) for local maxima trend lines
# see for mintrend above
# maxwindows - list of windows each containing maxtrend for that window
#returns (list of minima indexes, list of maxima indexes, [support slope coefficient, intersect], [resistance slope coefficient, intersect], [[support point indexes], (slope, intercept, residual, slope error, intercept error, area on wrong side of trend line per time unit)]

# The get_extrema function will calculate all of the local minima and local maxima without performing the full trend line calculation.

mins, maxs = trendln.calc_support_resistance(h, accuracy = accuracy)
print("On close price: calc_support_resistance")
print("mins")
print(type(mins))
print(mins)

total = len(mins[0])
for i in range(total):
    # print(i)
    # print(mins[0][i])
    print(idx[mins[0][i]])
    print(h.iloc[mins[0][i]])

# print("date")
# print(idx[mins[0][15]])
# print("price")
# print(h.iloc[mins[0][15]])

# print("date")
# print(idx[mins[2][0][0][1]])
# print("price")
# print(h.iloc[mins[2][0][0][1]])

print("maxs")
# print(type(maxs))
print(maxs)

total = len(maxs[0])
for i in range(total):
    # print(i)
    # print(maxs[0][i])
    print(idx[maxs[0][i]])
    print(h.iloc[maxs[0][i]])

minimaIdxs, pmin, mintrend, minwindows = trendln.calc_support_resistance((hist[numberOfDays:].Low, None), accuracy = accuracy) #support only
print("On low price: calc_support_resistance")
print("minimaIdxs")
print(minimaIdxs)
print("pmin")
print(pmin)
print("mintrend")
print(mintrend)
print("minwindows")
print(minwindows)

mins, maxs = trendln.calc_support_resistance((hist[numberOfDays:].Low, hist[numberOfDays:].High), accuracy = accuracy)
print("On low and high price: calc_support_resistance")
print("mins")
print(mins)
print("maxs")
print(maxs)

(minimaIdxs, pmin, mintrend, minwindows), (maximaIdxs, pmax, maxtrend, maxwindows) = mins, maxs
minimaIdxs, maximaIdxs = trendln.get_extrema(hist[numberOfDays:].Close, accuracy = accuracy)
print("On close price: get_extrema")
print("minimaIdxs")
print(minimaIdxs)
print("maximaIdxs")
print(maximaIdxs)

# The plot_support_resistance function will calculate and plot the average and top 2 support and resistance lines, along with marking extrema used with a maximum history length, and otherwise identical arguments to the calculation function.
# plt = trendln.plot_support_resistance(hist[numberOfDays:].Close, accuracy = accuracy) # requires matplotlib - pip install matplotlib
# plt.savefig("close_" + fileName, format=format)
# plt.show()
# plt.clf() #clear figure

maximaIdxs = trendln.get_extrema((None, hist[numberOfDays:].High), accuracy = accuracy) #maxima only
print("On high price: get_extrema")
print("minimaIdxs")
print(minimaIdxs)
print("maximaIdxs")
print(maximaIdxs)

minimaIdxs, maximaIdxs = trendln.get_extrema((hist[numberOfDays:].Low, hist[numberOfDays:].High), accuracy = accuracy)
print("On low price: get_extrema")
print("minimaIdxs")
print(minimaIdxs)
print("maximaIdxs")
print(maximaIdxs)

plt = trendln.plot_sup_res_date(hist[numberOfDays:].Close, idx, accuracy = accuracy) #requires pandas
plt.savefig("close_" + fileName, format=format)
plt.show()
plt.clf() #clear figure

plt = trendln.plot_sup_res_date((hist[numberOfDays:].Low, hist[numberOfDays:].High), idx, accuracy = accuracy) #requires pandas
plt.savefig("low_high_" + fileName, format=format)
plt.show()
plt.clf() #clear figure

# trendln.plot_sup_res_learn('.', hist)